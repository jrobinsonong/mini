exports.Dynamic=class Dynamic{
    constructor(obj,item){
        var h=window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;
        var w=window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
        var base_width=obj.state[item].width||0;
        var base_height=obj.state[item].height||0;
        this.WGap=w-base_width;
        this.HGap=h-base_height;
        this.item=item;
        this.obj=obj;
    }
    resize=()=>{
        var h=window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;
        var w=window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
        let update={[this.item]:{}};
        if(w-this.WGap>0)update[this.item].width=w-this.WGap;
        if(h-this.HGap>0)update[this.item].height=h-this.HGap;
        this.obj.setState(update);
    }
}

let Msg="Hello Robinson";
exports.Alert=()=>{
    alert(Msg);
}
let ObjectReader=data=>{
    let str='',item=[];
    if(typeof data==="string")str+=data;
    else {
        let Keys=Object.keys(data);
        for(let x in Keys){
            let Idx=Keys[x];
            let Val=data[Idx];
            str+='<'+Idx;
            if(typeof Val==="string")str+='>'+Val;
            else {
                if(Array.isArray(Val)){
                    str+='>';
                    item=Val.map(ObjectReader);
                    for(let x in item)  str+=item[x];
                }
                else{
                    let Property=Object.keys(Val);
                    for(let x in Property){
                        if(Property[x]!=='html'){
                            if(Array.isArray(Val[Property[x]])){
                                let style=Val[Property[x]],css='';
                                style.map(data=>{
                                    let cssKeys=Object.keys(data);
                                    for(let x in cssKeys)   css+=`${cssKeys[x]}:${data[cssKeys[x]]};`
                                    return data;
                                })
                                if(css!=='')str+=` ${Property[x]}="${css}"`;        
                            }else str+=` ${Property[x]}="${Val[Property[x]]}"` 
                        }
                    }
                    str+='>';
                    let {html}=Val;
                    if(html)str+=`${html}`;
                } 
            }
            str+='</'+Idx+'>';
        }
    }
    return str;
}

exports.Compiler=data=>{
    let str='',item=[];
    item=data.map(ObjectReader)
    item.map(data=>{
        str+=data
        return data;
    });
    
    return str;
}
/*
let ReactReader=data=>{
    let str='',item=[];
    if(typeof data==="string")str+=data;
    else {
        let Keys=Object.keys(data);
        for(let x in Keys){
            let Idx=Keys[x];
            let Val=data[Idx];
            str+='<'+Idx;
            if(typeof Val==="string")str+='>'+Val;
            else {
                if(Array.isArray(Val)){
                    str+='>';
                    item=Val.map(ObjectReader);
                    for(let x in item)  str+=item[x];
                }
                else{
                    let Property=Object.keys(Val);
                    for(let x in Property){
                        if(Property[x]!=='html'){
                            if(Array.isArray(Val[Property[x]])){
                                let style=Val[Property[x]],css='';
                                style.map(data=>{
                                    let cssKeys=Object.keys(data);
                                    for(let x in cssKeys)   css+=`${cssKeys[x]}:${data[cssKeys[x]]};`
                                    return data;
                                })
                                if(css!=='')str+=` ${Property[x]}="${css}"`;        
                            }else str+=` ${Property[x]}="${Val[Property[x]]}"` 
                        }
                    }
                    str+='>';
                    let {html}=Val;
                    if(html)str+=`${html}`;
                } 
            }
            str+='</'+Idx+'>';
        }
    }
    return str;
}

exports.ReactCompiler=data=>{

    return '<div>Hello</div>'
}*/