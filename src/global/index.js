const action =  require('./action');
const data   =  require('./data');

let AKeys=Object.keys(action.default);
AKeys.map(key=>{
    exports[key]=action.default[key]
    return key;
});

let DKeys=Object.keys(data.default);
DKeys.map(key=>{
    exports[key]=data.default[key]
    return key;
});
