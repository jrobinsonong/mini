import {SESSION} from './constant';
import API from './../api';
let {User} = API;

let action={
    loadSession:()=>{
        let data=JSON.parse(localStorage.getItem(SESSION))||{}
        return data||{};
    },
    saveSession:(data,file)=>{
        let storage=JSON.parse(localStorage.getItem(SESSION))||{}
        if( !storage[file] )storage[file]={};
        storage[file]={...storage[file],...data};
        localStorage.setItem(SESSION, JSON.stringify(storage));
    },
    home_action:{
        load:(obj,page)=>{
            obj.props['home']?obj.props.home({page}):obj.home({page});
            action.saveSession({page},'home'); 
        }
    },
    registration_action:{
        register:obj=>{
            console.log(obj.state)
            action.user_action.save(obj.state);
            action.home_action.load(obj,"Login");
        },
        clear:obj=>{
            let Keys=Object.keys(obj.state);
            for(let x in Keys){
                let index=Keys[x];
                obj.state[index]='';
            }
            obj.registration(obj);
        },
    },
    login_action:{
        
    },
    user_action:{
        getUser:()=>User.Get(),
        save:data=>User.Add(data),
        update:(filter,data)=>User.Update(filter,data),
        delete:filter=>User.Delete(filter),
        reset:()=>User.Reset(),
        
    },
    main_action:{
        load:(obj,data)=>{
            obj.main(data);
            obj.props['main']?obj.props.main(data):obj.main(data);
            action.saveSession({page:data.page},'main');
        }
    }
}
export default action