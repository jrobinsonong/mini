import React from 'react'

function Input(props){
    let {Label,Name,change,Type}=props
    let value=props.state?props.state[Name]:"";
    Type=Type||'text';
    return(
        <div className="w3-col s12 w3-pale-blue w3-container" style={{fontSize:"80%"}}>
            <span className="w3-left">{Label} : </span>
            <input type={Type} className="w3-input w3-border" name={Name} onChange={change} value={value}/>
        </div>
    )
}

export default Input