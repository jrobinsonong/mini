import React from 'react';
function Button(props){
    return(
        <div className="w3-border w3-ripple w3-container w3-blue w3-hover-pale-blue" 
        style={{width:"auto",cursor:"pointer",fontWeight:"bold"}}>{props.Label}</div>
    )

}
export default Button