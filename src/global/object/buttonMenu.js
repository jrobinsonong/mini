import React from 'react'
import {main_action}        from '../../global/';
let {load} = main_action;
function buttonMenu(props){
    let {Label,menu}=props;
    return(
        <div className="w3-col s4 w3-border w3-ripple w3-button" 
            onClick={()=>load(props,{display:menu,page:Label})}>{Label}</div>
    )
}

export default buttonMenu