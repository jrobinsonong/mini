// import Therion from "__src/commons/core";
import Msg from "./Messaging";
import _GraphqlRequest from "./lib/GraphqlRequest";
import RequestManager from "./lib/RequestManager";
// import * as modelDefs from "__src/commons/models";
// import * as custom from "__src/config/custom";
import * as connection from "./config/connections";
import * as environment from "./config/environment";

let _authApiUrl = "";

let _msgApiUrl = "";
let _liveStreamUrl="";
let _apiKey = "";
let _app = "";

switch (environment.mode){
case environment.PRODUCTION:
	_authApiUrl = connection.prodAuthApiUrl;
	_msgApiUrl = connection.prodMsgApiUrl;

	break;
case environment.STAGING:
	_authApiUrl = connection.stagingAuthApiUrl;
	_msgApiUrl = connection.stagingMsgApiUrl;
	break;
case environment.TEST:
	_authApiUrl = connection.testAuthApiUrl;
	_msgApiUrl = connection.testMsgApiUrl;
	_liveStreamUrl = connection.testLiveStreamUrl;
	_apiKey = "992348d0d1217f81e82bd36b5533939227de43bd7031d545b43824df913e650e";
	_app = "com.gomigu.unified-1"
	break;
case environment.DEVELOPMENT:
default:
	_authApiUrl = connection.devAuthApiUrl;
	_msgApiUrl = connection.devMsgApiUrl;
	break;
}

export const authApiUrl = _authApiUrl;
export const msgApiUrl = _msgApiUrl;
export const apiKey = _apiKey;
export const app = _app;

console.debug("###########");
console.debug(`Authentication Server: ${authApiUrl}`);
console.debug(`Messaging Server: ${msgApiUrl}`);
console.debug("###########");


export const Messaging = new Msg({host: _msgApiUrl, apiKey: _apiKey, app: _app});

// export const therion = new Therion(modelDefs, { host: _authApiUrl });
export const GraphqlRequest = new _GraphqlRequest({ host: _authApiUrl, apiKey: _apiKey, app: _app });
export const MsgApiService = new RequestManager({ host: _msgApiUrl, apiKey: _apiKey, app: _app });

export const setToken = (token) => {
	MsgApiService.setToken(token);
	Messaging.setToken(token);
};
