export const STAGING = "staging";
export const PRODUCTION = "production";
export const TEST = "test";
export const DEVELOPMENT = "development";

/* eslint-disable */
let mode_dev;
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
	mode_dev = TEST;
} else {
	mode_dev = PRODUCTION;
}

export const mode = mode_dev;
