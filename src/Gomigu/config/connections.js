//
// Authorization service
//
export const testAuthApiUrl = "https://test-auth.gomigu.com";
//
// Messaging service
//
export const testMsgApiUrl = "https://test-msg.gomigu.com";

export const testLiveStreamUrl="z";

export const devAuthApiUrl="";

export const devMsgApiUrl="";

export const prodAuthApiUrl="";

export const prodMsgApiUrl="";

export const stagingAuthApiUrl="";
export const stagingMsgApiUrl="";