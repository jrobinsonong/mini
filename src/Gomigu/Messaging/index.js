import _ from "lodash";
import moment from "moment"; 
import validUrl from "valid-url";
import hash from "hash.js";
// import async from "async"
// import bluebird from "bluebird";

import SocketIO, {
	CONNECT,
	CONNECT_TIMEOUT,
	DISCONNECT,
	CONNECT_ERROR,
} from "../lib/SocketIO";
import RequestManager from "../lib/RequestManager";
import * as custom from "./custom";
import * as Events from "./events";
import * as enums from "../config/enums";
import * as globals from "../globals";
let userId,config;
const listenerDispatchers = [
	Events.MESSAGE_NEW,
	Events.MESSAGE_SEEN,

	Events.CHANNEL_CREATED,
	Events.CHANNEL_UPDATED,
	Events.CHANNEL_INVITED,
	Events.CHANNEL_DELETED,

	Events.USER_ACTIVE,
	Events.USER_INACTIVE,
	Events.USER_ONLINE,
	Events.USER_OFFLINE,
	Events.USER_BLOCKED,
	Events.USER_UNBLOCKED,
	Events.USER_UPDATED,

	Events.SENDER_TYPING,
	Events.SENDER_IDLE,
];

const noop = () => new Promise((resolve) => resolve());

class Messaging {
	constructor(config) {
		this.apiKey = config.apiKey;
		this.app = config.app;
		this.host = config.host;
	}
	reconnectCount = 0;

	_debounceUpdateDeviceSession = _.debounce(async(appNextState, deviceId, userId) => {
		try {
			let hidden = "hidden";

			if ("hidden" in document) {
				hidden = "hidden";
			} else if ("mozHidden" in document) {
				hidden = "mozHidden";
			} else if ("webkitHidden" in document) {
				hidden = "webkitHidden";
			} else if ("msHidden" in document) {
				hidden = "msHidden";
			}

			console.debug(`AppState::isHidden ${document[hidden]}`);
			await this.request
				.patch(`/api/v1/updateStatus/${111}`, {
					isOnline: true,
					isActive: !document["hidden"],
					userId,
					platform: "WEB",
					deviceId,
				});
		} catch (error) {
			console.error("AppState::error", error);
		}

	}, custom.appStateListenInterval);

	// Login and registers user and calls connect to socket
	// Login object must contain username, firstName, lastName where username is upsid
	// Onevent is handleMessagingEvents(userId, dispatch), in utils
	loginConnect = async(loginObj, webpush, onEvent = noop) => {
		await this._loginOrRegister(loginObj)

		this.connect(webpush, onEvent);
	}

	connect = (webpush, onEvent = noop) => {
		this.reconnect = true;
		this.reconnectCount = 0;

		this._connect(webpush, userId, onEvent);
	}

	// username should be userId
	_loginOrRegister = async(loginObj) => {
		const { username, firstName, lastName } = loginObj;		
		const result = await globals.GraphqlRequest.Api.create("Session", {
			values: {
				username,
				firstName,
				lastName,
				orgName: "Unified Marketplace",
			},
			options: { returning: true },
		}, [
			"id",
			"accessToken",
			"refreshToken",
			"userId",
			[
				"user",
				[
					"firstName",
					"middleName",
					"lastName",
					
				],
			],
		]);

		this.setToken(result.accessToken);
		this._userId = result.id;
	}

	_connect = (webpush, onEvent = noop) => {
		this.host = globals.msgApiUrl;
		this._webpush = webpush;
		// this._userId = userId;

		console.debug("CONNECT>>>>");
		// Connect to the socket
		const handshake =
			`apiKey=${this.apiKey}&` +
			`deviceId=${webpush}&` +
			`accessToken=${this.token}&` +
			"platform=WEB&" +
			`userId=${this._userId ? this._userId : userId}`;

		console.debug(handshake);
		console.debug("CheckReconnect::handshake", this.reconnect);

		this.socketIO = new SocketIO(this.host, handshake, false,
			async (event, data) => {
				switch (event){
				case CONNECT:
					this.reconnectCount = 0;

					if (!this.reconnect){
						return this.disconnect;
					}

					// config.io = data;
					this.io = data;
					this.request = new RequestManager(config);
					this.requestHttp = new RequestManager(config);

					this.request.setToken(this.token);
					await this._handleOnConnected(webpush, userId, onEvent);
					break;
				case DISCONNECT:
					await this._handleOnDisconnected();

					// eslint-disable-next-line no-fallthrough
				case CONNECT_TIMEOUT:
				case CONNECT_ERROR:
					this.request = null;
					// Handle previously failed intentional disconnect
					console.debug("CheckReconnect::error", this.reconnect);
					if (!this.reconnect) {
						return this.disconnect();
					}
					// Reconnect to socket
					if (this._connectTimeout) {
						clearTimeout(this._connectTimeout);
						console.debug("Messaging::Connect::ClearTimeout", this._connectTimeout);
					}

					console.debug("Messaging::Connect::Reconnect", this._connectTimeout);
					this.reconnectCount++;
					this._connectTimeout = setTimeout(() => {
						this._connect(config, webpush, onEvent);
					}, custom.socketReconnectInterval);
					break;
				default:
					console.debug(`Event: ${event} | Data: ${data}`);
				}

				onEvent(event, data);
			}
		);

		this._declareIdle = (userActivity) => {
			setTimeout(() => {
				onEvent(Events.SENDER_IDLE, userActivity);
			}, custom.typingMsgTimeout);
		};
	}

	getUserId = () => this._userId;

	setUserId = (userId) => {
		if (_.isNil(this.socketIO)){
			this._userId = userId;
		}
	};

	setToken = (token) => {
		this.token = token;

		if (this.request) {
			this.request.setToken(token);
		}
	}

	setRefreshToken = (refreshToken) => {
		this.refreshToken = refreshToken;
		if (this.request) {
			this.request.setRefreshToken(refreshToken);
		}
	}

	disconnect = () => {
		console.debug("Socket::disconnect");
		this.reconnect = false;
		this.socketIO.disconnect();
	}

	//
	// Checks if the current socket is connected
	//
	isConnected = () => this.io && this.io.socket && this.io.socket.isConnected();

	//
	// Broadcast a message to all channel subscribers
	//
	broadcast = async (message) => {

		//
		// Do not proceed if there's nothing to send
		//
		if (_.isEmpty(message.message) && !message.attachments.length && message.type !== enums.SHARE &&
		_.isEmpty(message.scrapedMessage) && _.isEmpty(message.streamKey)) {
			return;
		}

		// Make sure senderId is not undefined
		message.senderId = this._userId;

		console.log("lib::Messaging::broadcast message", message);

		const newMessage = await this.request.post("/api/v1/messages", message);

		// //
		// // Inform 3rd party that user is idle
		// //
		// if (newMessage.channelId) {
		// 	await this.idle(newMessage.channelId);
		// }

		console.log("lib::Messaging::broadcast newMessage", newMessage);

		return newMessage;
	}

	joinChannel = async (channelId) => {
		console.debug("lib::Messaging::joinChannel");
		if (this._userId) {
			const subscription = await this.request.post("/api/v1/subscriptions", {
				subscriberId: this._userId,
				channelId,
			});

			return subscription;
		}
	}

	getSubscription = async (channel, userId) => {
		console.debug("lib::Messaging::getSubscription");
		const subscription = await this.request.get("/api/v1/subscriptions", {
			where: {
				subscriberId: this._userId,
				channel,
				type: [enums.GROUP, enums.DIRECT],
			},
			skip: 0,
			limit: 1,
		});

		return subscription;
	}

	createMessage = async (message, type, referenceId) => {
		const attachments = _.transform(message.attachments, (result, attachment) => {
			if (attachment instanceof Blob || validUrl.isUri(attachment.uri)) {
				result.push(attachment);
			}
		}, []);

		const msgObj = {
			...message,
			type,
			sentAt: +moment(),
			senderId: this._userId,
			attachments,
			// receiverId: message.receiverId,
			// channelId: message.channelId,
			// teamId: message.teamId,
			// location: message.location,
			// taggedUsers: message.taggedUsers,
		};

		if ( type === enums.SHARE || type === enums.COMMENT || type === enums.COMMENT_REPLY ||
		type === enums.MESSAGE_REPLY || type === enums.REPORT){
			msgObj.referenceId = referenceId;
			// msgObj.sender = user;
		}

		// if (type === enums.POST) {
		// 	msgObj.sender = user;
		// }

		msgObj.hash = await this._hashMessage(msgObj);
		msgObj.hashId = msgObj.hash;

		return msgObj;
	}

	_hashMessage = (message) => (
		new Promise((resolve) => {
			//
			// Create a unique hash for this message
			//

			const identifier = message.receiverId ? message.receiverId : message.channelId;
			const raw =
				`${message.senderId},` +
				`${identifier},` +
				`${message.message},` +
				`${JSON.stringify(message.attachments)},` +
				`${message.sentAt}`;

			const hashValue = hash.sha256().update(raw).digest("hex");

			resolve(hashValue);
		})
	)

	_registerAppStateListener = async (deviceId, userId) => {
		try {
			// const deviceSessions = await this.request.get("/api/v1/deviceSessions", {
			// 	where: {
			// 		deviceId,
			// 		platform: "WEB",
			// 		userId,
			// 		expiresAt: { ">": +moment() },
			// 	},
			// 	limit: 10,
			// 	skip: 0,
			// });

			try {
				let change = "visibilitychange";

				if ("hidden" in document) {
					change = "visibilitychange";
				} else if ("mozHidden" in document) {
					change = "mozvisibilitychange";
				} else if ("webkitHidden" in document) {
					change = "webkitvisibilitychange";
				} else if ("msHidden" in document) {
					change = "msvisibilitychange";
				}

				if (this._appStateListener) {
					document.removeEventListener(change, this._appStateListener);
				}

				this._appStateListener = async (appNextState) => {
					this._debounceUpdateDeviceSession(appNextState, deviceId, userId);
				};

				document.addEventListener(change, this._appStateListener);

			} catch (e) {
				console.error("RegisterAppListener::saveAppListener::error", e);
			}
		} catch (error) {
			console.error("RegisterAppListener::error", error);
		}
	}

	_handleOnConnected = async (deviceId, userId, onEvent) => {
		try {
			console.debug("Messaging::_handleOnConnected");

			this.joinChannel();
			this._registerAppStateListener(deviceId, userId);

			//
			// Register listeners for socket events
			//
			_.forEach(listenerDispatchers, (key) => {
				console.debug("listenerDispatchers", key);
				this.io.socket.on(key, (data) => {

					console.log("HAYY ==>", key, data);

					switch (key) {
					case Events.SENDER_TYPING:
						this._declareIdle(data);
						break;
					default:
						break;
					}

					onEvent(key, data);
				});
			});
		} catch (e) {
			console.error("_handleOnConnected ==> Error");
			console.error(e);
		}
	}

	_handleOnDisconnected = async(appName, apiKey) => {
		console.debug("Messaging::_handleOnDisconnected");
		if (this._appStateListener) {
			let change = "visibilitychange";

			if ("hidden" in document) {
				change = "visibilitychange";
			} else if ("mozHidden" in document) {
				change = "mozvisibilitychange";
			} else if ("webkitHidden" in document) {
				change = "webkitvisibilitychange";
			} else if ("msHidden" in document) {
				change = "msvisibilitychange";
			}

			document.removeEventListener(change, this._appStateListener);
		}
		this.io = null;
	}

	getReconnectCount = () => this.reconnectCount;

	getThreads = async (skip, limit, sort) => {
		const threads = await this._getChannelByType("THREADS", skip, limit, sort);

		return _.transform(threads, (result, thread) => {
			thread.lastMsgId = thread.lastMessage ? thread.lastMessage.id : "";
			result[thread.id] = thread;
		}, {});
	}

	getFiltered = async (filter, skip, limit, sort) => {
		const threads = await this._getChannelByType("THREADS", skip, limit, sort, filter);

		return _.transform(threads, (result, thread) => {
			thread.lastMsgId = thread.lastMessage ? thread.lastMessage.id : "";
			result[thread.id] = thread;
		}, {});
	}

	getChannels = async (skip, limit, sort) => {
		return this._getChannelByType("All", skip, limit, sort);
	}

	listMessages = async (where, skip = 0, limit = 30, sort = [{ updatedAt: "DESC" }]) => {
		console.debug("lib::Messaging::listMessages");
		const messages = await this.request.get("/api/v1/messages", {
			where,
			skip,
			limit,
			sort,
		});

		return messages;
	}

	getSearchThread = async (data, type, skip = 0, limit = 15, sort =
	[{ updatedAt: "DESC"}]) => {
		let filter;

		if (type === "all") {
			filter = ["GROUP", "DIRECT"];
		} else if (type === "direct") {
			filter = "DIRECT";
		} else {
			filter = "GROUP";
		}

		const result = await this.request.get("/api/v1/channels", {
			where: {
				type: filter,
				search: data,
				subscriberId: this._userId,
			},
			skip,
			limit,
			sort,
			// where: {
			// 	type: filter,
			// 	or: [ {
			// 		name: {
			// 			contains: data,
			// 		},
			// 	},
			// 	{
			// 		longName: {
			// 			contains: data,
			// 		},
			// 	},
			// 	],
			// 	subscriberId: this._userId,
			// },
		});

		return result;
	}

	_getChannelByType = async (type, skip = 0, limit = 15, sort =
	[{ updatedAt: "DESC"}], filter = "") => {
		let where, filtered;

		if (type === "THREADS") {
			switch (filter){
			case "direct":
				filtered = [enums.DIRECT, enums.ACCEPTED_MESSAGE_REQUEST];
				break;
			case "group":
				filtered = enums.GROUP;
				break;
			default:
				filtered = [enums.GROUP, enums.DIRECT, enums.ACCEPTED_MESSAGE_REQUEST];
				break;
			}
			where = {
				subscriberId: this._userId,
				type: filtered,
				$or: [
					{ lastMessage: { "!=": null }},
					{ lastMessage: { "!=": "" }},
				],
			};
		} else {
			where = {
				createdById: this._userId,
				type: [enums.NEWSFEED, "TIMELINE"],
			};
		}

		const result = await this.request.get("/api/v1/channels", {
			where,
			skip,
			limit,
			sort,
		});

		// console.log("lib::Messaging::getChannelsByType", type, result);

		return result;
	}

	reactOnPost = async (reaction, messageId) => {
		const reactObj = await this.request.post("/api/v1/reactions", {
			reaction,
			reactorId: this._userId,
			messageId,
		});

		return reactObj;
	}

	updateReactOnPost = async (reaction, reactionId) => {
		const reactObj = await this.request.patch(`/api/v1/reactions/${reactionId}`, {
			reaction,
		});

		return reactObj;
	}

	deleteReactOnPost = async (reactorId, messageId) => {
		const reactObj = await this.request.delete(`/api/v1/reactions/${reactorId}`, {
			reactorId,
			messageId,
		});

		return reactObj;
	}

	createChannel = async (channelBody) => {
		const result = await this.request.post("/api/v1/channels", channelBody);

		return result;
	}

	seenAll = async (channelId, lastMsgId) => {
		const result = await this.request.patch(`/api/v1/messages/${lastMsgId}`, {
			userId: this._userId,
			action: "read_all",
			channelId,
		});

		return result;
	}

	getNewsfeed = async (where, skip = 0, limit = 30, sort = [{ updatedAt: "DESC" }]) => {
		const result = await this.request.get("/api/v1/newsfeeds", {
			where,
			skip,
			limit,
			sort,
		});

		return result;
	}

	saveGroupPhoto = async (groupId, groupPhoto) => {
		const result = await this.request.patch(`/api/v1/channels/${groupId}`, {
			groupPhoto,
		});

		return result;
	}

	updateMessageRequest = async (channelId, type) => {
		const result = await this.request.patch(`/api/v1/channels/${channelId}`, {
			action: "update_message_request",
			type,
		});

		return result;
	}

	updateGroupInfo = async (groupId, data) => {
		const result = await this.request.patch(`/api/v1/channels/${groupId}`, {
			name: data.name,
			description: data.description,
		});

		return result;
	}

	leaveGroup = async (groupId, data) => {
		await this.request.patch(`/api/v1/channels/${groupId}`, {
			action: "remove_members",
			memberIds: data,
		});

		const threads = await this.getThreads();

		return _.transform(threads, (result, thread) => {
			thread.lastMsgId = thread.lastMessage ? thread.lastMessage.id : "";
			result[thread.id] = thread;
		}, {});;
	}

	addMemberToGroup = async (groupId, data) => {
		const result = await this.request.patch(`/api/v1/channels/${groupId}`, {
			action:  "add_members",
			memberIds: data,
		});

		return result;
	}

	updateMemberRole = async (subscriptionId, role) => {
		const result = await this.request.patch(`/api/v1/subscriptions/${subscriptionId}`, {
			role,
		});

		return result;
	}

	removeMemberFromGroup = async (groupId, data) => {
		const result = await this.request.patch(`/api/v1/channels/${groupId}`, {
			action: "remove_members",
			memberIds: data,
		});

		return result;
	}

	unsubscribeUserFromGroup = async (channelId, subscriberId) => {

		await this.request.post("/api/v1/subscriptions", {
			unsubscribe: true,
			channelId,
			subscriberId,
		});
	};

	updateContactSubscription = async (subscriberId, channelId) => {
		console.debug("lib::Messaging::joinChannel");
		const subscription = await this.request.post("/api/v1/subscriptions", {
			subscriberId,
			channelId,
		});

		return subscription;
	}

	getChannelData = async (channelId) => {
		const result = await this.requestHttp.get("/api/v1/channels", {
			where: { id: channelId },
		});

		return result[0];
	}

	getDirectChannel = async (channelId) => {
		if (!channelId) {
			return;
		}

		const channel = await this.request.get("/api/v1/channels/", {
			where: {
				type: enums.DIRECT,
				id: channelId,
				// or: [
				// 	{
				// 		createdById: contactId,
				// 		peerId: this._userId,
				// 	},
				// 	{
				// 		createdById: this._userId,
				// 		peerId: contactId,
				// 	},
				// ],
			},
		});

		return channel[0];
	}

	updatePostPrivacySettings = async (messageId, value = enums.PUBLIC) => {
		if (!messageId) {
			return;
		}

		const result = await this.request.patch(`/api/v1/messages/${messageId}`, {
			privacy: value,
		});

		return result;
	}

	getGroupSubscribers = async (where, skip, limit, sort) => {
		const result = await this.request.get("/api/v1/subscriptions", {
			where,
			skip,
			limit: limit ? limit : 4,
		});

		return result;
	}

	getSearchedThreadShare = async (e, subscriberId, type) => {
		const result = await this.request.get("/api/v1/channels/", {
			where: {
				search: e,
				subscriberId,
				type,
			},
		});

		return result;
	}

	updateMessage = async (messageId, data) => {
		const result = await this.request.patch(`/api/v1/messages/${messageId}`, {
			action: "edit",
			...data,
		});

		return result;
	}

	getMessage = async (messageId) => {
		const result = await this.request.patch(`/api/v1/messages/${messageId}`);

		return result;
	}

	generateStreamKey = async (userId) => {
		console.debug("lib::Messaging::getStreamKey");
		const result = await this.request.post("/api/v1/livestreams", {
			action: "create_key",
			userId,
		});

		return result;
	}

	connectStream = async (streamKey) => {
		try {
			console.debug("lib::Messaging::connectingToStream");
			const result = await this.request.post("/api/v1/livestreams", {
				action: "pre_join",
				streamKey,
			});

			return result;
		} catch (err) {
			console.debug("Message:ConnectStream:failed", err);

			// reconnect if fails
			setTimeout(() => {
				this.connectStream(streamKey);
			}, 1000);
		}
	}

	joinStream = async (streamKey, userId) => {
		try {
			console.debug("lib::Messaging::joiningStream");
			const result = await this.request.post("/api/v1/livestreams", {
				action: "join",
				streamKey,
				userId,
			});

			return result;
		} catch (err) {
			console.debug("Message:joinStream:failed", err);
		}
	}

	leaveStream = async (streamKey, userId) => {
		try {
			console.debug("lib::Messaging::leavingStream");
			const result = await this.request.post("/api/v1/livestreams", {
				action: "leave",
				streamKey,
				userId,
			});

			return result;
		} catch (err) {
			console.debug("Message:leaveStream:failed", err);
		}
	}

	endLiveStream = async (streamKey, platform) => {
		try {
			console.debug("lib::Messaging::endLiveStream");
			const result = await this.request.post("/api/v1/livestreams", {
				action: "end",
				platform,
				streamKey,
			});

			return result;
		} catch (err) {
			console.debug("Message:endLiveStream:failed", err);
		}
	}

	pushStream = async (streamKey, pushUrl) => {
		try {
			console.debug("lib::Messaging::pushStream");
			const result = await this.request.post("/api/v1/livestreams", {
				action: "dynamic_push",
				platform: "WEB",
				pushUrl,
				streamKey,
			});

			console.log("yahoo 123 ==>", result);

			return result;
		} catch (err) {
			console.debug("Message:pushStream:failed", err);
		}
	}
}

export const MsgEvents = Events;
export default Messaging;