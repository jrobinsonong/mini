
//
// Splash screen timeout (ms)
//
export const splashTimeout = 3000;

//
// Typing message timeout (ms)
//
export const typingMsgTimeout = 3000;

//
// Typing Debounce (ms)
//
export const typingDebounce = typingMsgTimeout / 3;

//
// Debounce thread reloads (ms)
//
export const reloadThread = 1000;

//
// Send message debounce (ms)
//
export const sendMessageDebounce = 1000;

//
// Socket reconnect interval (ms)
//
export const socketReconnectInterval = 5000;
//
// Appstate Listen (ms)
//
export const appStateListenInterval = 3000;
