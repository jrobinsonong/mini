//
// SOCKET SERVER
//
export const CONNECTED = "event/CONNECTED";
export const CONNECTION_ERROR = "event/CONNECTION_ERROR";
export const CONNECTION_TIMEOUT = "event/CONNECTION_TIMEOUT";
export const DISCONNECTED = "event/DISCONNECTED";
export const RECONNECTED = "event/RECONNECTED";
export const RECONNECTING = "event/RECONNECTING";
export const RECONNECT_ATTEMPTED = "event/RECONNECT_ATTEMPTED";
export const RECONNECT_ERROR = "event/RECONNECT_ERROR";
export const RECONNECT_FAILED = "event/RECONNECT_FAILED";
export const ERROR = "event/ERROR";
export const PING = "event/PING";
export const PONG = "event/PONG";

//
// MESSAGING
//
export const USER_ACTIVE = "event/USER_ACTIVE";
export const USER_INACTIVE = "event/USER_INACTIVE";
export const USER_ONLINE = "event/USER_ONLINE";
export const USER_OFFLINE = "event/USER_OFFLINE";
export const USER_BLOCKED = "event/USER_BLOCKED";
export const USER_UNBLOCKED = "event/USER_UNBLOCKED";
export const USER_UPDATED = "event/USER_UPDATED";

export const SENDER_TYPING = "event/SENDER_TYPING";
export const SENDER_IDLE = "event/SENDER_IDLE";

export const MESSAGE_NEW = "event/MESSAGE_NEW";
export const MESSAGE_SEEN = "event/MESSAGE_SEEN";

export const CHANNEL_CREATED = "event/CHANNEL_CREATED";
export const CHANNEL_UPDATED = "event/CHANNEL_UPDATED";
export const CHANNEL_DELETED = "event/CHANNEL_DELETED";
export const CHANNEL_INVITED = "event/CHANNEL_INVITED";
