import Immutable, { fromJS, List, OrderedMap } from "immutable";
import moment from "moment";
import _ from "lodash";

import { MsgEvents } from "__src/api/Messaging";
import * as SocketEvents from "__src/lib/SocketIO";
import * as Types from "./types";
import * as globals from "__src/globals";
import * as enums from "__src/config/enums";
import cryptojs from "crypto-js";
import security from "__src/config/security";

const { isList } = List;

const listenerDispatchers = {
	[SocketEvents.CONNECT]: Types.CONNECTED,
	[SocketEvents.DISCONNECT]: Types.DISCONNECTED,
	[SocketEvents.CONNECT_ERROR]: Types.CONNECTION_ERROR,
	[SocketEvents.CONNECT_TIMEOUT]: Types.CONNECTION_TIMEOUT,
	[SocketEvents.RECONNECTING]: Types.RECONNECTING,
	[SocketEvents.RECONNECT]: Types.RECONNECTED,
	[SocketEvents.RECONNECT_ERROR]: Types.RECONNECTION_ERROR,
	[SocketEvents.RECONNECT_FAILED]: Types.RECONNECTION_FAILED,

	[MsgEvents.USER_ACTIVE]: Types.USER_ACTIVE,
	[MsgEvents.USER_INACTIVE]: Types.USER_INACTIVE,
	[MsgEvents.USER_ONLINE]: Types.USER_ONLINE,
	[MsgEvents.USER_OFFLINE]: Types.USER_OFFLINE,
	[MsgEvents.USER_BLOCKED]: Types.USER_BLOCKED,
	[MsgEvents.USER_UNBLOCKED]: Types.USER_UNBLOCKED,
	[MsgEvents.USER_UPDATED]: Types.USER_UPDATED,
	[MsgEvents.SENDER_TYPING]: Types.SENDER_TYPING,
	[MsgEvents.SENDER_IDLE]: Types.SENDER_IDLE,
	[MsgEvents.MESSAGE_NEW]: Types.MESSAGE_NEW,
	[MsgEvents.MESSAGE_SEEN]: Types.MESSAGE_SEEN,
	[MsgEvents.CHANNEL_CREATED]: Types.CHANNEL_CREATED,
	[MsgEvents.CHANNEL_UPDATED]: Types.CHANNEL_UPDATED,
	[MsgEvents.CHANNEL_DELETED]: Types.CHANNEL_DELETED,
	[MsgEvents.CHANNEL_INVITED]: Types.CHANNEL_INVITED,
};

export const handleMessagingEvents = (userId, dispatch, loadThreads) => (
	// eslint-disable-next-line max-statements
	async (event, data) => {
		try {
			const type = listenerDispatchers[event];
			const userId = globals.Messaging.getUserId();

			switch (type){
			case Types.CONNECTED:
				console.debug("HomeScreen::onEvent ==> Types.CONNECTED!");
				loadThreads();
				break;
			case Types.MESSAGE_NEW:
				console.debug("HomeScreen::onEvent ==> Types.MESSAGE_NEW!");

				const result = await globals.Messaging.getChannelData(data.channel.id);

				data.subscriptions = result.subscriptions;
				data.channel.subscriptions = result.subscriptions;
				if (_.isEmpty(data.channel.longName) &&
					_.isEmpty(data.channel.name)){

					data.channel.longName = generateLongName(result.subscriptions);
				}

				if (data.reference !== null) {
					const userId = globals.Messaging.getUserId();

					let identifier = data.channel.id;

					if (data.type === enums.COMMENT && data.channel.type !== enums.NEWSFEED) {
						identifier = getIdentifierId(data.channel, userId);

						return dispatch({
							type: Types.NEW_COMMENT,
							data: {
								comment: data,
								messageId: data.reference.id,
								identifier,
								messageHash: data.reference.hash,
								sending: true,
							},
						});
					} else if (data.type === enums.COMMENT_REPLY && data.channel.type !== enums.NEWSFEED) {
						identifier = getIdentifierId(data.channel, userId);

						return dispatch({
							type: Types.NEW_REPLY_COMMENT,
							data: {
								identifier,
								message: data,
								commentHash: data.reference.hash,
								reference: data.reference,
							},
						});
					}
				}

				let directChannel = {};

				if (data.channel.type === enums.DIRECT) {
					directChannel = await globals.Messaging.getDirectChannel(data.channel.id);
				}

				if (directChannel.id) {
					data.channel = directChannel;
				}

				break;
			case Types.CHANNEL_INVITED: {
				const userId = globals.Messaging.getUserId();

				console.debug("HomeScreen::onEvent ==> Types.CHANNEL_INVITE", data);
				const channel = data;

				await globals.Messaging.joinChannel(channel.id);

				console.debug(`User has joined channel (${channel.id})`);
				console.debug(channel);

				const result = await globals.Messaging.getChannelData(channel.id);

				if (channel.type === "PENDING_MESSAGE_REQUEST") {
					if (channel.createdById !== userId) {
						return dispatch({
							type: Types.LOAD_MESSAGE_REQUEST_SUCCESS,
							data: {
								messageRequests: [result],
								isPending: true,
								skip: 0,
								limit: 15,
								firstLoad: false,
							},
						});
					}

					return;
				}

				if (_.isEmpty(result.longName) && _.isEmpty(result.name)){
					const longName = generateLongName(result.subscriptions);

					result.longName = longName;
				}

				return dispatch({ type, data: result });
			}
			case Types.CHANNEL_UPDATED: {
				const result = await globals.Messaging.getChannelData(data.id);

				return dispatch({
					type,
					data: result,
				});
			}
			case Types.MESSAGE_SEEN: {
				if (!data || data.type === enums.COMMENT || data.type === enums.COMMENT_REPLY) {
					return;
				}

				return dispatch({ type, data });
			}
			case Types.USER_ONLINE:
			case Types.USER_OFFLINE: {
				return dispatch({ type, data });
			}
			default:
				console.debug("utils::handleMessagingEvents");
				console.debug(type);
				console.debug(event);
				console.debug(data);
				console.debug("=====");
				break;
			}

			dispatch({ type, data });
		} catch (err) {
			console.error("utils::handleMessagingEvents::error", err);
		}
	}
);