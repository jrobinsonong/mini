import * as globals from "__src/globals";
import * as enums from "__src/config/enums";

// const MODEL = "subscriptions";

const Channels = {
	retrieveContact: async (contactId, userId, isPrivate = false) => {
		console.debug("Channel::retrieveContact");
		const subscription = await globals.MsgApiService.get("/api/v1/getContact", {
			where: {
				userId,
				contactId,
				isPrivate,
			},
			skip: 0,
			sort: [{ updatedAt: "DESC"}],
			limit: 3,
		});

		return subscription;
	},
};

export default Channels;
