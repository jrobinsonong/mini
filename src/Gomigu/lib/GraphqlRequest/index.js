import Api from "./managers/Api";
import RequestManager from "../RequestManager";

class GraphqlRequest {
	constructor(config) {
		this._requestManager = new RequestManager(config);
		this._api = new Api(this._requestManager);
	}

	get Api() {
		return this._api;
	}

	get RequestManager() {
		return this._requestManager;
	}
}

export default GraphqlRequest;
