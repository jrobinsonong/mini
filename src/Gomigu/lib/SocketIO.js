import socketIOClient from "socket.io-client";
import sailsIOClient from "sails.io.js";

export const CONNECT = "connect";
export const CONNECT_ERROR = "connect_error";
export const CONNECT_TIMEOUT = "connect_timeout";
export const ERROR = "error";
export const DISCONNECT = "disconnect";
export const RECONNECT = "reconnect";
export const RECONNECT_ATTEMPT = "reconnect_attempt";
export const RECONNECTING = "reconnecting";
export const RECONNECT_ERROR = "reconnect_error";
export const RECONNECT_FAILED = "reconnect_failed";
export const PING = "ping";
export const PONG = "pong";

const socketEvents = [
	CONNECT,
	CONNECT_ERROR,
	CONNECT_TIMEOUT,
	ERROR,
	DISCONNECT,
	RECONNECT,
	RECONNECT_ATTEMPT,
	RECONNECTING,
	RECONNECT_ERROR,
	RECONNECT_FAILED,
	PING,
	PONG,
];

const noop = () => new Promise((resolve) => resolve());

//
// Ensure that sails-socket-io is initialized only once
//
let socketIO;


class SocketIO {

	constructor(url, handshake, reconnect = true, onEvent = noop) {
		// let manualConnect = true;

		if (!socketIO) {
			socketIO = sailsIOClient(socketIOClient);
			// manualConnect = false;
		}

		this.io = socketIO;
		this.disconnect();

		this.io.sails.query = handshake;
		this.io.sails.reconnection = reconnect;
		this.io.sails.environment = "production";
		this.io.sails.url = url;
		this.io.sails.useCORSRouteToGetCookie = false;
		console.debug(`Attempting to connect to ${this.io.sails.url}...`);
		console.debug(this.io.sails);

		this.socket = this.io.sails.connect();
		this.io.socket = this.socket;

		for (const socketEvent of socketEvents) {
			this.io.socket.on(socketEvent, this._onSocketEvent(socketEvent, onEvent));
		}

		// if (manualConnect) {
		// 	try {
		// 		this.io.socket.reconnect();
		// 	} catch (e) {
		// 		// Fail silently
		// 	}
		// }
	}

	on = (eventName, cb) => this.io.socket.on(eventName, cb);

	disconnect = () => {
		try {
			this.io.socket.removeAllListeners();
			this.io.socket.disconnect();
		} catch (e) {
			// Fail silently
		}
	}

	_onSocketEvent = (event, onEvent) => {
		return async (data) => {
			switch (event) {
			case CONNECT:
				console.debug("Device has established a connection to the messaging server!");
				data = this.io;
				break;
			case CONNECT_ERROR:
				console.debug("Error connecting with the server!");
				break;
			case DISCONNECT:
				console.debug("Device has disconnected from the messaging server!");
				break;
			default:
				break;
			}

			console.debug("Socket event: ", event);
			console.debug("Socket data: ", data);
			await onEvent(event, data);
		};
	}
}

export default SocketIO;
