/* eslint-disable no-console */
import _ from "lodash";

class Request {
	constructor(config) {
		this._url = (config.port) ? `${config.host}:${config.port}` : config.host;
		this._appName = config.appName;
		this._apiKey = config.apiKey;
		this._token = config.token;
		this._headers = config.headers;
		this.io = config.io;
	}

	setToken = (token) => this._token = token;

	get = async (route, query = {}) => await this._request(route, "GET", query);

	post = async (route, body) => await this._request(route, "POST", body);

	patch = async (route, body) => await this._request(route, "PATCH", body);

	delete = async (route, body) => await this._request(route, "DELETE", body);

	// eslint-disable-next-line max-statements
	_request = async (route, method, values) => {
		try {
			let url = `${this._url}${route}`;
			const payload = {
				method,
				headers: {
					...this._headers,
					Accept: "application/json",
					"Content-Type": "application/json",
				},
			};

			if (method !== "GET") {
				if (this.io) {
					payload.params = values;
				} else {
					payload.body = JSON.stringify(values);
				}
			}

			if (this._appName) {
				payload.headers["x-app"] = this._appName;
			}

			if (this._apiKey) {
				payload.headers["x-api-key"] = this._apiKey;
			}

			if (this._token) {
				payload.headers["Authorization"] = `Bearer ${this._token}`;
			}

			if (method === "GET" && !_.isEmpty(values)) {
				url += "?";

				_.forEach(values, (v, k) => {
					url += `${k}=${this._prepareQuery(v)}&`;
				});
			}

			console.debug(payload);

			if (this.io) {
				return await this._sendSocketRequest(url, payload);
			}

			return await this._sendHttpRequest(url, payload);
		} catch (e) {
			console.log(e);

			throw e;
		}
	}

	_sendHttpRequest = async (url, payload) => {
		const response = await fetch(url, payload);

		if (response.status !== 200) {
			throw new Error(response);
		}

		return await response.json();
	}

	_sendSocketRequest = async (url, payload) => {
		try {
			console.log("_sendSocketRequest", url, payload);

			return await new Promise((resolve, reject) => {
				payload.url = url;
				this.io.socket.request(payload, (resData, jwres) => {
					console.log("Reqeust:result", jwres);
					console.log("Reqeust:statusCode", jwres.statusCode);
					console.log("Reqeust:condt", jwres.statusCode !== 200);
					if (jwres.statusCode !== 200) {
						console.log("Reqeust:here", jwres);

						return reject(jwres);
					}

					return resolve(resData);
				});
			});
		} catch (e) {
			console.debug(e);

			throw e;
		}
	}

	_prepareQuery = (query) => (encodeURIComponent(JSON.stringify(query)));
}

export default Request;