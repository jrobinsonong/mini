// import Script from './../global/script';
class Beta {
    constructor(fields,collection){
        this.fields     =   fields      || [];
        this.collection =   collection  || 'Temp';
        this.data       =   JSON.parse(localStorage.getItem(this.collection)) || [];
        
    }
    Add = data =>{
        let size=this.data.length,newData={};
        this.fields.map(field=>{
            newData[field]=data[field] || '';
            return field;
        });
        newData.id=size > 0 ? Number(this.data[size-1].id)+ 1 : 1; 
        newData.status='New'; 
        this.data.push(newData);
        localStorage.setItem(this.collection, JSON.stringify(this.data));
    }
    Delete = index =>{
        this.data[index]=undefined;
        localStorage.setItem(this.collection, JSON.stringify(this.data));
    }
    Update = (index,data) =>{
        let newData={}
        this.fields.map(field=>{
            newData[field]=data[field] || '';
            return field;
        });
        this.data[index]=newData;
        localStorage.setItem(this.collection, JSON.stringify(this.data));
    }
    Reset = () =>{
        this.data=[];
        localStorage.removeItem(this.collection);
    } 
    Get     = index         => index?this.data[index]:this.data;
    Fields  = ()            => this.fields
    Count   = ()            => this.data.length;
}
export default Beta;