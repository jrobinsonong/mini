import React,{Component}    from 'react';
import {home_action}        from '../../global';
let {load} = home_action;

export default class extends Component{
    
    render(){
        return(
            <div className=" w3-light-grey" style={{height:"100%"}}>
                <div className="w3-center"><br/>
                    <img src="https://messaging.unified.ph/system.ico" alt="Avatar" style={{width:"150px"}} className="w3-circle w3-margin-top"/>
                </div>
                <form id="LoginForm" className="w3-container">
                    <div className="w3-section">
                        <label className="w3-left"><b>User Name :</b></label>
                        <input className="w3-input w3-border w3-margin-bottom UserName" type="text" placeholder="Enter User Name" required/>
                        <label className="w3-left"><b>Password :</b></label>
                        <input className="w3-input w3-border Password" type="password" placeholder="Enter Password" required onChange={this.submit}/>
                        <div className="w3-button w3-block w3-green w3-section w3-padding btnLogin" onClick={()=>load(this,'Main')}>Login</div>
                        <div className="w3-button w3-block w3-orange w3-section w3-padding btnGuest" onClick={()=>load(this,'Main')}>Login in as Guest</div>
                    </div>
                </form>
                <div className="w3-container w3-border-top w3-padding-16">
                    <div className="w3-button w3-blue w3-right btnSignUp" onClick={()=>load(this,'Registration')}>Sign Up</div>
                </div>
            </div>    
        )
    }
}