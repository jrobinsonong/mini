import React,{Component}    from 'react';
import Registration         from '../registration';
import Login                from '../login';
import Main                 from '../main';
import {home,loadSession}   from '../../global/';
import {Dynamic}            from '../../global/script';
export default class extends Component{
    constructor(props){
        super(props);
        this.state=home;
        
    }
    home=obj=>this.setState(obj);
    // action={...this.props,...{home:this.home}}
    PlatForm;
    
    static getDerivedStateFromProps(props, state) {
        let {home}=loadSession();
        return home?home:state;
    }
    
    check=()=>this.PlatForm.resize();
    
    componentDidMount() {
        
        this.PlatForm=new Dynamic(this,'PlatForm');
        window.addEventListener("resize", this.check);
    }
    render(){
        let {page,PlatForm}=this.state,current;
        switch(page){
            case 'Main':
                current=<Main {...this}/>
                break;
            case 'Registration':
                current=<Registration {...this}/>
                break;
            default:
                current=<Login {...this}/>
        }
        return(
            <center>
                <div className="w3-card-4" style={{maxWidth:"400px",height:PlatForm.height}}>
                    {current}
                </div>
            </center>
        )        
    }    

};