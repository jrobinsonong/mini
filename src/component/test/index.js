import React,{Component}            from 'react';
import {Button}                     from '../../global/object';
import {Compiler}     from '../../global/script';
// import ReactHtmlParser from 'react-html-parser';
export default class extends Component{
    constructor(props){
        super(props)
        this.state={
            Display:"",
            React:""
        };
    }
    converter=data=>{
        let str=<div>Result</div>
        let newData=str;
        return newData;
    }
    componentDidMount(){
        let data=[
            {div:[
                {h1:{
                    html:"Hello World",
                    style:[{color:"Red"}]
                }},
                {hr:""},
                {div:[{Button:{onClick:"this.Alert",html:"Save"}}]},
                {div:[{h3:"By Robinson"}]}
            ]}
        ];
        console.log("Data:",data)
        console.log("Result:",Compiler(data));
        this.setState({Display:Compiler(data)});
        let newData=[{div:"Hello"}];
        
        
        this.setState({React:this.converter(newData)})
    }
    
    // function createMarkup() {
    // return {__html: 'First &middot; Second'};
    // }
    Alert=()=>{
        alert("Robinson!!!");
    }
    Display=data=>{
        return {__html: data}
    }
    // function MyComponent() {
    // return <div dangerouslySetInnerHTML={createMarkup()} />;
    // }
    render(){
        return(
            <div>
                Welcome to Testing Site
                <hr/>
                {/* <Input Label="Test" icon="SaveOutlined" {...this}/> */}
                <button onClick={this.Alert}>Legit</button>
                <Button Label="Proto Type"{...this}></Button>
                <div dangerouslySetInnerHTML={this.Display(this.state.Display)} />
                {this.state.Display}
                {this.state.React}
            </div>
        )
    }
}