import React,{Component}                from 'react';
import {user_action,registration}       from '../../global/';
import {Input}                          from '../../global/object';
import { Button }                       from 'antd';

export default class extends Component{
    constructor(props){
        super(props);
        this.state=registration;
    }
    change=e=>{
        this.setState({[e.target.name]:e.target.value})
    }
    save=()=>{
        user_action.save(this.state);
        this.getRecords();
    }
    getRecords=()=>{
        let data=user_action.getUser();
        console.log("Data:",data);
    }
    clear=()=>{
        this.setState(registration);
        // user_action.reset();
        // this.getRecords();
    }
    render(){
        return(
            <div style={{height:"100%"}} className="w3-pale-blue">
                <div className="w3-border w3-col s6">Registration</div>
                <div className="w3-border w3-col s6">List</div>
                <div>
                    <Input Name="UserName"  {...this} Label="User Name"/>
                    <Input Name="Password"  {...this} Label="Password"          Type="password"/>
                    <Input Name="Pass2"     {...this} Label="Re Type Password"  Type="password" />
                    <Input Name="FirstName" {...this} Label="First Name"/>
                    <Input Name="LastName"  {...this} Label="Last Name"/>
                    <Input Name="NickName"  {...this} Label="Nick Name"/>
                </div>
                <Button type="primary" onClick={this.save}>Register</Button>
                <Button type="danger" onClick={this.clear}>Clear</Button>
                
                {/* <button onClick={this.getRecords}>Data</button>
                <button onClick={this.clear}>Reset</button> */}
            </div>
        )
    }
}