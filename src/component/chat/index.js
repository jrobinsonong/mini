import React,{Component}            from 'react';
import mySocket from 'sails-socket'
const Local='http://localhost:1337';
const VPN='http://10.10.1.45:1337';
let Server = VPN;
    // Server = Local;
mySocket.connect({url:Server,reconnection:true})
//|=========================| Listening Socket |========================|//
//|=====================================================================|//
export default class extends Component{
    constructor(props){
        super(props)
        this.state={
            UserName:'',
            Online:[],
            Msg:'',
            Convo:[],
        };
    }
    //|===================================================================================================================================|//
    componentDidMount(){
        //|----------------------------| Auto Reconnect |-----------------------|//
        mySocket.on('reconnect', ()=>{
            console.log('Reconnect')
            this.Login();
        });
        // //|----------------------------| G-Message Receiving |------------------|//
        mySocket.on('Blast', res=>{
            console.log(res);
        });
        // //|----------------------------| Message Receiving |--------------------|//
        mySocket.on('Msg', res=>{
            const {Convo}=this.state;
            Convo.push(res);
            this.setState({Convo})
            // console.log('Msg:',res);
        });
        // //|----------------------------| Ping |---------------------------------|//
        mySocket.on('Session', ()=> {
            const {UserName} = this.state;
            if(UserName!=='')mySocket.post(`${Server}/chat/Ping`,{Name:UserName});
        });
        // //|----------------------------| Online List |--------------------------|//
        mySocket.on('Online',res=>{
            // console.log("Online:",res);
            this.setState({Online:res})
        });
        //|---------------------------------------------------------------------|//
    }
    //|----------------------------| Login |--------------------------------|//
    Login=()=>{
        const {UserName}=this.state;
        if(UserName!==''){
            mySocket.post(`${Server}/chat/Login`,{Name:UserName},res=>{
                console.log(res);
            }); 
        }
    }
    //|----------------------------| Send Message |-------------------------|//
    SendMsg=(Receiver)=>{
        const {UserName,Msg,Convo}=this.state;
        const data={Sender:UserName,Receiver,Msg};
        console.log("Sending Data:",data)
        Convo.push(data);
        this.setState({Convo})
        mySocket.post(`${Server}/chat/Send`,data); 
    }
    //|----------------------------| Display Online |-----------------------|//
    DisplayOnline=()=>{
        const {Online,UserName}=this.state;
        const online=Online.map(data=>{
            const {User,ID}=data;
            const button=User!==UserName?
                <input type="button" value="Send" style={{width:"100%"}} onClick={()=>this.SendMsg(ID)} key={ID}/>:
                null
            return (
                <tr key={`Row-${ID}`}><td className="w3-border">{User}</td>
                    <td className="w3-border">{button}</td>
                </tr>)
        })
        return online;
    }
    //|----------------------------| Display Chat |-------------------------|//
    DisplayChat=()=>{
        const {Convo}=this.state;
        const convo=Convo.map(data=>{
            const {Sender,Msg}=data;
            return(
                <tr key={`Row-${Math.random()}`}><td>{Sender}:<br/>
                    {Msg}
                </td></tr>)
        });
        return convo;
    }
    //|---------------------------------------------------------------------|//
    change=e=>{
        const {name,value}=e.target
        this.setState({[name]:value},()=>{
            if(name==='UserName'){
                console.log(value);
                this.Login();
            }
        });
    }
    //|---------------------------------------------------------------------|//
    render(){
        const Online=this.DisplayOnline();
        const Convo=this.DisplayChat();
        return(
            <div>
                <table width="100%" border="0">
                    <thead>
                    <tr><td>
                        <table border="1" width="100%">
                            <tbody>
                                <tr><td><input type="text" name="UserName" style={{width:"100%"}} 
                                    placeholder="Sender Name" onBlur={this.change}/></td></tr>
                                <tr><td><textarea placeholder="Write Your Message Here..." name="Msg"
                                    style={{width:"100%"}} onBlur={this.change}></textarea></td></tr>
                                <tr><td>Online User's</td></tr>
                                <tr><td><div style={{overflow: "auto", height:"200px"}}>
                                    <table id="OnlineList" style={{width:"100%"}}><tbody>{Online}</tbody></table>
                                </div></td></tr>
                            </tbody>
                        </table></td></tr>
                    </thead>
                    <tbody>
                        <tr><td valign="top"><div style={{overflow: "auto", height:"200px"}} className="w3-border">
                            <table border="1" id="Conversation" width="100%"><tbody>{Convo}</tbody></table>
                        </div></td></tr>
                    </tbody>    
                </table>
            </div>
        )
    }
}