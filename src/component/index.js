exports.Home=require('./home').default;
exports.Login=require('./login').default;
exports.Main=require('./main').default;
exports.Registration=require('./registration').default;
exports.Test=require('./test').default;
exports.User=require('./user').default;
exports.Chat=require('./chat').default;