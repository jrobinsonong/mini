import React,{Component}                from 'react';
import {ButtonMenu}                     from '../../global/object'; 
import {User,Test, Chat}                from '../../component';
import {home_action,main,loadSession}   from '../../global/';

let {load} = home_action;
export default class extends Component{
    constructor(props){
        super(props)
        this.state=main;
    }
    main=obj=>this.setState(obj);
    static getDerivedStateFromProps(props, state) {
        let {main}=loadSession();
        return main?main:state;
    }
    render(){
        let {display,page,frame}=this.state,loadPage;
        let menu=display==='none'?'block':'none';
        switch(page){
            case "Profile":   case "Friends":   case "Settings":    case 'Change Logs':
                loadPage=<Test {...this}/>
            break;
            case "Chat Room":   loadPage=<Chat {...this}/>
            break;
            default:
                loadPage=<User {...this}/>;
        }
        return(
            <div style={{height:"100%"}}>
                <div className="w3-col s8  w3-padding  w3-border w3-pale-blue"><b>Smartweb</b></div>
                <div className="w3-col s4 w3-pale-red w3-padding w3-ripple w3-border" 
                    onClick={()=>load(this,'Login')} style={{cursor:"pointer"}}>Log Out</div>
                <div className="w3-border w3-ripple w3-blue" style={{cursor:"pointer"}} 
                    onMouseEnter={()=>this.main({display:"block"})}
                    onClick={()=>this.main({display:menu})}>Menu </div>
                <div className="w3-row w3-pale-blue" style={{position:"absolute",width:"400px",display:display}}
                    onMouseLeave={()=>this.main({display:"none"})}>
                    <ButtonMenu {...this}{...{menu}} Label="Users"/>
                    <ButtonMenu {...this}{...{menu}} Label="Profile"/>
                    <ButtonMenu {...this}{...{menu}} Label="Friends"/>
                    <ButtonMenu {...this}{...{menu}} Label="Chat Room"/>
                    <ButtonMenu {...this}{...{menu}} Label="Settings"/>
                    <ButtonMenu {...this}{...{menu}} Label="Change Logs"/>
                </div>
                <div className="w3-center w3-border" style={{height:frame.height,overflow:"auto"}}>
                    {loadPage}                   
                </div>
            </div>
        )
    }
}