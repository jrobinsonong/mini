import React        from 'react';
import {registration,home_action,registration_action}         from '../../global';

let {load}          = home_action;
let {register,clear}= registration_action;
class Registration extends React.Component{
    constructor(props){
        super(props);
        this.state=registration;
        
    }
    registration=obj=>this.setState(obj);
    change=e=>this.setState({[e.target.name]:e.target.value});
    render(){
        let {UserName,Password,Password2,FirstName,LastName,NickName}=this.state;
        return(
            <div style={{height:"100%"}}>
                <form id="LoginForm" className="w3-container">
                    <h3>Registration</h3>
                    <div className="w3-section" style={{fontSize:"80%"}}>
                        <label className="w3-left"><b>User Name :</b></label>
                        <input className="w3-input w3-border w3-margin-bottom RegUserName" type="text" placeholder="Enter User Name" required onChange={this.change} name="UserName" value={UserName}/>
                        <label className="w3-left"><b>Password :</b></label>
                        <input className="w3-input w3-border RegPassword" type="password" placeholder="Enter Password" required onChange={this.change} name="Password" value={Password}/>
                        <label className="w3-left"><b>Re-type Password :</b></label>
                        <input className="w3-input w3-border RegPassword2" type="password" placeholder="Enter Password" required onChange={this.change} name="Password2" value={Password2}/>
                        <hr style={{height:"5px"}} className="w3-blue"/>
                        <label className="w3-left"><b>First Name :</b></label>
                        <input className="w3-input w3-border FirstName" type="text" placeholder="First Name" required onChange={this.change} name="FirstName" value={FirstName}/><br/>
                        <label className="w3-left"><b>Last Name :</b></label>
                        <input className="w3-input w3-border LastName" type="text" placeholder="Last Name" required onChange={this.change} name="LastName" value={LastName}/><br/>
                        <label className="w3-left"><b>Nick Name :</b></label>
                        <input className="w3-input w3-border NickName" type="text" placeholder="Nick Name" required onChange={this.change} name="NickName" value={NickName}/>
                        <div className="w3-button w3-block w3-green w3-section w3-padding" onClick={()=>register(this)}>Register</div>
                        <div className="w3-button w3-block w3-red w3-section w3-padding w3-col s6" onClick={()=>clear(this)}>Clear</div>
                        <div className="w3-button w3-block w3-pale-red w3-section w3-padding w3-col s6" onClick={()=>load(this,'Login')} >CLOSE</div>
                    </div>
                </form>
            </div>
        )
    }
}

export default Registration